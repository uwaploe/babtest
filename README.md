# Test DP BAB communications

Babtest provides a command-line interface to test communication between the DPC and the Battery Aggregator Board.

## Installation

Download the latest compressed TAR archive from the Downloads section and copy to the DPC. Unpack the archive and copy the binary to the `~/bin` directory.

## Usage

``` shellsession
(dp)rsn@rsn-dpc-2:~$ babtest --help
NAME:
   babtest - Test communication with the Battery Aggregator Board

USAGE:
   babtest [global options] command [command options] [arguments...]

VERSION:
   v0.1.0-g817f83f

COMMANDS:
   getave   Output average battery values
   getall   Output values from all batteries
   getch    Output values from a single battery
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --dev value                      BAB serial device (default: "/dev/ttyAT6") [$BAB_DEVICE]
   --timeout DURATION, -t DURATION  timeout DURATION (default: 10s)
   --baud value                     BAB baud rate (default: 4800)
   --version, -v                    print the version
   --help, -h                       show help
```

Each subcommand matches the command that is sent to the BAB. The responses are written to standard output as newline-delimited JSON.

### $GETAVE

``` shellsession
(dp)rsn@rsn-dpc-2:~$ babtest getave
{"count":8,"voltage":13249,"amperage":0,"capacity":101}
```

### $GETCH

``` shellsession
(dp)rsn@rsn-dpc-2:~$ babtest getch 4
{"channel":4,"voltage":13239,"amperage":0,"capacity":103}
```

### $GETALL

``` shellsession
(dp)rsn@rsn-dpc-2:~$ babtest getall
{"channel":0,"voltage":13241,"amperage":0,"capacity":101}
{"channel":1,"voltage":13234,"amperage":0,"capacity":101}
{"channel":2,"voltage":0,"amperage":0,"capacity":0}
{"channel":3,"voltage":0,"amperage":0,"capacity":0}
{"channel":4,"voltage":13238,"amperage":0,"capacity":103}
{"channel":5,"voltage":13237,"amperage":0,"capacity":102}
{"channel":6,"voltage":13235,"amperage":0,"capacity":102}
{"channel":7,"voltage":13252,"amperage":0,"capacity":101}
{"channel":8,"voltage":13254,"amperage":0,"capacity":101}
{"channel":9,"voltage":13256,"amperage":0,"capacity":100}
```
