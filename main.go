// Babtest provides a command line interface to test communication with the
// Battery Aggregator Board.
package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/bab"
	"github.com/urfave/cli"
	"go.bug.st/serial"
)

var Version = "dev"
var BuildDate = "unknown"

func main() {
	app := cli.NewApp()
	app.Name = "babtest"
	app.Usage = "Test communication with the Battery Aggregator Board"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "dev",
			Usage:  "BAB serial device",
			Value:  "/dev/ttyAT6",
			EnvVar: "BAB_DEVICE",
		},
		cli.DurationFlag{
			Name:  "timeout, t",
			Usage: "timeout `DURATION`",
			Value: time.Second * 10,
		},
		cli.IntFlag{
			Name:   "baud",
			Usage:  "BAB baud rate",
			Value:  4800,
			EnvVar: "BAB_BAUD",
		},
		cli.VersionFlag,
	}

	var dev *bab.Device
	app.Before = func(c *cli.Context) error {
		mode := &serial.Mode{
			BaudRate: c.Int("baud"),
			DataBits: 8,
			StopBits: serial.OneStopBit,
		}
		port, err := serial.Open(c.String("dev"), mode)
		if err != nil {
			return err
		}
		if dt := c.Duration("timeout"); dt > 0 {
			port.SetReadTimeout(dt)
		}

		dev = bab.New(port)
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:  "getave",
			Usage: "Output average battery values",
			Action: func(c *cli.Context) error {
				summary, err := dev.GetAve()
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Command failed: %v", err), 2)
				}
				json.NewEncoder(os.Stdout).Encode(summary)
				return nil
			},
		},
		{
			Name:  "getall",
			Usage: "Output values from all batteries",
			Action: func(c *cli.Context) error {
				states, err := dev.GetAll()
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Command failed: %v", err), 2)
				}
				enc := json.NewEncoder(os.Stdout)
				for _, state := range states {
					enc.Encode(state)
				}
				return nil
			},
		},
		{
			Name:      "getch",
			Usage:     "Output values from a single battery",
			ArgsUsage: "CHANNEL",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing channel number", 1)
				}
				channel, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					return cli.NewExitError("cannot parse channel value", 1)
				}
				state, err := dev.GetCh(channel)
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Command failed: %v", err), 2)
				}
				json.NewEncoder(os.Stdout).Encode(state)
				return nil
			},
		},
	}

	app.Run(os.Args)
}
