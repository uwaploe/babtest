module bitbucket.org/uwaploe/babtest

go 1.16

require (
	bitbucket.org/uwaploe/bab v0.4.0
	github.com/urfave/cli v1.22.5
	go.bug.st/serial v1.5.0
)
